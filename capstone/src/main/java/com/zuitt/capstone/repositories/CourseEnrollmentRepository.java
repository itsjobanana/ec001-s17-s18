package com.zuitt.capstone.repositories;

import com.zuitt.capstone.models.CourseEnrollment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseEnrollmentRepository extends CrudRepository<CourseEnrollment, Object> {
}
